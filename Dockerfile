FROM ubuntu:latest
LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN apt-get update && apt-get install --yes curl

COPY sloppy /usr/local/bin/sloppy
RUN chmod +x /usr/local/bin/sloppy

CMD ["sloppy", "version"]